const mongoose = require("mongoose")

// create schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "Pending"
	}
})

// create model
// "module.exports"  is a way for our js to treat this value as a "package" that can be used by other files
module.exports = mongoose.model("Task",taskSchema)