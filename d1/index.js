// set up the dependencies
const express = require("express")
const mongoose = require("mongoose")

// this allows us to use all the routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute")




// Server setup
const app = express()
const port = 3001;
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// mongoDB connection
// connectig mongoDB atlas
// change password and db name
mongoose.connect("mongodb+srv://hazelg21:hlg10212016@wdc028-course-booking.x4c4d.mongodb.net/batch144-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology:true
})

// to check if mongoDB connection is successful
let db = mongoose.connection;
// if error occured, output in the console
db.on("error", console.error.bind(console,"connection error"))
// but if the connection is successful, output in the console
db.once("open",() => console.log("We're connected to the cloud database."))

// end of mongoose connection code


// this will be the base URI
// it allows all the task route created in the taskRoute.js to use the "/task"
app.use("/tasks",taskRoute)
//http:localhost:3001/tasks


app.listen(port,() => console.log(`Now listening to port ${port}`))
