const express = require("express")
const taskController = require("../controllers/taskController")

// allows us to access http methods: get push put delete
// create a router instance that functions as a middleware and routing system
// allows access to HTTP method middlewares
const router = express.Router()

// responsible for defining the URL/Route
//get("url",function)
//http:localhost:3001/tasks/
router.get("/",(request,response) => {
	taskController.getAllTask().then(resultFromController => response.send(resultFromController))
})



// create a route for creating a task
router.post("/",(request,response) => {
	taskController.createTask(request.body).then(result => response.send(result))
})


// route for deleting
// wildcard parameter /:id
// http://localhost:3001/tasks/:id
// the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// the word that comes after the colon symbol will be the name of the URL parameter
// ":id" is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
router.delete("/:id", (request,response) => {
	// URL parameter values are accessed via the request object's "params" property
	// the property name of this object will match the given URL parameter
	// in this case the "id" is the name of the parameter
	taskController.deleteTask(request.params.id).then(result => response.send(result))
})


// update a task
router.put("/:id",(request,response) => {
	taskController.updateTask(request.params.id,request.body).then(result => response.send(result))
})


// ACTIVITY==================

// specific task
router.get("/:id", (request,response) => {
	taskController.specificTask(request.params.id).then(result => response.send(result))
})


// change status to "complete"
router.patch("/:id/complete",(request,response) => {
	taskController.changeStatus(request.params.id).then(result => response.send(result))
})

module.exports = router;