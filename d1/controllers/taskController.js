// controllers contain the functions and business logic of our express app.
// all logic and operations are placed here

const Task = require('../models/task');

// controller function for getting all the content of the DB
module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}


// controller function for creating a task
module.exports.createTask = (requestBody) => {
	// create a task object based on the mongoose model "Task"
	let newTask = new Task ({
		name: requestBody.name
	})

	// save
	// we use 'return' since we use .then()
	// for .then first parameter is OK the second is for ERROR
	// the .then() accepts 2 parameters
	//	the first parameter will store the result returned by the mongoose "save" method
	// 	the second paremeter will store the "error" object
	return newTask.save().then((task,error) => {
		if (error){
			console.log(error)
			// if an error is encountered, the "return" statement will prevent any other line or code below and within the same code bloc from executing.
			// since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// the false statement will no longer evaluated
			return false;
		}
		else {
			// if save is successful, return the new task object
			return task;
		}
	})
}



// controller function for deleting a task
/*

business logic
1. look for the task with the corresponding id provided in the URL/route
2. delete task


*/

module.exports.deleteTask = (taskId) => {
	// "findByIdAndRemove" is a mongoose method
	// it will look for a task with the same id provided from the URL and remove/delete the document from MongoDB. it looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask,error) => {
		if (error) {
			console.log(error)
			return false;
		}
		else {
			return removedTask;
		}
	})
}


// Updating a task
/*
1. Get the task with the Id
2. Replace the task name return from the database with the name property from requestBody
3. save the task

*/



// update task
module.exports.updateTask = (taskId,newContent) => {
	return Task.findById(taskId).then((result,error) => {
		// if an error is encountered in finding the task using the ID, return a false
		if (error){
			console.log(error)
			return false
		} 


		// when no error encountered in finding the task using the ID the code below will be executed:
		// results of the "findById" method will be stored in the "result" parameter
		// its "name" property will be reassigned the value of the "name" received from the request

		result.name = newContent.name;
		return result.save().then((updatedTask,error) => {
			// when error in saving is encountered
			if (error) {
				console.log(error)
				return false;
			}
			else {
				// no error encountered in saving
				return updatedTask
			}
		})
		})
}


// ACTIVITY==================

// specific task
module.exports.specificTask = (taskId) => {
	return Task.findById(taskId).then((result,error) => {
		if (error){
			console.log(error)
			return false;
		}
		else{
			return result
		}
	})
}

// change status to "complete"
module.exports.changeStatus = (taskId) => {
	return Task.findByIdAndUpdate(taskId).then((result,error) => {
		if (error){
			console.log(error)
			return false;
		}
		else {
			result.status = "complete"
			return result.save().then((updatedStatus,error) => {
				if (error){
					console.log(error)
					return false;
				}
				else {
					return updatedStatus;
				}
			})
		}
	})

}